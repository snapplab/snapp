const Arguments = require('./arguments');
const acceptArgs = (name, fn) => (...args) => {
  let argsInstance = Arguments.construct(args, name);
  let result = fn.apply(argsInstance);
  argsInstance.verifyNoUnusedArguments();
  return result;
};

const {DEBUG} = require('./log');

module.exports = function (name) {
  DEBUG && DEBUG(`Loading core package ✰ ${name}`);
  switch (name) {
    case 'Framework':
      return {
        package: {
          load: acceptArgs('Framework.package.load', require('./framework/package/load'))
        }
      };
    case 'StringOperations':
      return {
        case: {
          param: acceptArgs('StringOperations.case.param', require('./string-operations/case/param'))
        }
      };
    case 'RegularExpressions':
      return {
        case: {
          transition: {
            lowerToUpper: require('./regular-expressions/case/transition/lower-to-upper')
          }
        }
      };
    default:
      throw new Error(`${name} is not a core package, try reinstalling Snapp`);
  }
}
