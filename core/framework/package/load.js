const fs = require('fs');
const path = require('path');
const {execSync} = require('child_process');

const Arguments = require('../../arguments');
const {StringOperations} = require('../../../');
const {WARN, INFO, DEBUG, SILLY} = require('../../log');

const home = process.env.SNAPP_HOME;
const repository = process.env.SNAPP_REPOSITORY || 'https://repository.snapplab.com';
const repositoryPath = process.env.SNAPP_REPOSITORY_PATH;

const autoLoadProxy = function (base, pkgName, baseName='', depth=0) {
  SILLY && SILLY('Level', depth, base, pkgName);
  let currentPath = path.join(base, StringOperations.case.param(pkgName));
  let levelProxy;
  let validated = false;
  let level;

  let fullName = baseName ? `${baseName}.${pkgName}` : pkgName;

  try {
    level = require(currentPath);
    if (pkgName === 'construct' && typeof level !== 'function') {
      throw new Error(`Hey, ${fullName} must be a function`);
    }
    INFO && INFO(`Loaded package ✰ ${fullName}`);
  }

  catch (e) {
    if (!e.message.match(/^Cannot find module/)) {
      throw e;
    }
    if (pkgName === 'construct') {
      throw new Error(`No ${fullName}() found, cannot construct instance`);
    }
    level = function (...args) {
      if (this instanceof Arguments) {
        args = [this];
      }
      let instanceData = {};
      let instance = new Proxy(instanceData, {
        get: function (target, name) {
          if (name === 'constructor') {
            return levelProxy;
          }
          if (instanceData.hasOwnProperty(name)) {
            return instanceData[name];
          }
          let result = levelProxy.prototypeGet(levelProxy, name, instance);
          if (typeof result === 'function') {
            result = result.bind(instance);
          }
          return result;
        },
        set: function (target, name, value) {
          instanceData[name] = value;
        }
      });
      SILLY && SILLY('Constructing', `${fullName}(${args.map(Arguments.formatArg).join(', ')})`);
      levelProxy.construct.apply(instance, args);
      return instance;
    };
    try {
      let describe = require(path.join(currentPath, 'description-of'));
      level.descriptionOf = function () {
        return describe(this);
      };
      SILLY && SILLY('Good, descriptionOf found for module', fullName);
    }

    catch (e) {
      SILLY && SILLY('No descriptionOf found for module', fullName);
      level.descriptionOf = null;
    }
    DEBUG && DEBUG(`Loaded package ❒ ${fullName}`);
  }

  if (typeof level !== 'function') {
    return level;
  }

  let prototypeGet = function (target, name, instance) {
    if (name === 'prototypeGet') {
      return prototypeGet;
    }
    if (name === 'prototype') {
      return levelProxy;
    }
    if (name === 'descriptionOf') {
      return level[name];
    }
    if (name === 'toString' || name === Symbol.toPrimitive || name === 'inspect') {
      return function () {
        if (this === levelProxy) {
          return fullName;
        }
        let desc = '';
        let space = '';
        try {
          desc = this.descriptionOf();
          if (desc && desc.length > 0) {
            space = ' ';
          }
        }
        catch (e) {
          desc = Object.keys(this).map(key => {
            return [key, typeof this[key]].join(': ');
          }).join(', ');
          desc = `{${desc}}`;
        }
        return `‹${fullName}${space}${desc}›`;
      }
    }

    if (!(name in level)) {
      if (instance && instance.hasOwnProperty('resolveDirectory')) {
        currentPath = instance.resolveDirectory.path;
      }
      let next = autoLoadProxy(currentPath, name, fullName, depth + 1);
      if (typeof next === 'function') {
        level[name] = function (...args) {
          let argsInstance
          if (args.length && args[0] instanceof Arguments) {
            argsInstance = args[0];
          }
            else {
            argsInstance = Arguments.construct(args, `${fullName}.${name}`);
          }
          let result = next.call(argsInstance, this);
          argsInstance.verifyNoUnusedArguments();
          return result;
        }
      }
      else {
        level[name] = next;
      }
    }

    return level[name];
  };

  levelProxy = new Proxy(level, {get: prototypeGet});
  return levelProxy;
};

let installLocal = name => {
  let paramCaseName = StringOperations.case.param(name)
  let fromPath = path.join(repositoryPath, paramCaseName, 'src');
  let toPath = path.join(home, paramCaseName);
  try {
    execSync(`ln -s ${fromPath} ${toPath}`);
  }
  catch (e) {
    throw new Error(`Package ${name} failed to install from ${fromPath}, aborting`);
  }
}

let installRemote = name => {
  let url = `${repository}/${StringOperations.case.param(name)}`;
  WARN && WARN(`Package ${name} not found locally, installing from ${url}`);
  let fullPath = path.join(home, StringOperations.case.param(name));
  try {
    execSync(`curl ${url} > ${fullPath}.tar.gz`);
  }
  catch (e) {
    execSync(`rm -f ${fullPath}.tar.gz`);
    throw new Error(`Package ${name} failed to install from ${url}, aborting`);
  }
};

module.exports = function () {
  let packages = this.object;
  return name => {
    if (!(name in packages)) {
      try {
        fs.readdirSync(path.join(home, StringOperations.case.param(name)));
      }
      catch (e) {
        if (repositoryPath) {
          installLocal(name);
        }
        else {
          installRemote(name);
        }
      }

      packages[name] = autoLoadProxy(home, name);
    }
    return packages[name];
  };
};
