const {formatArg} = require('./arguments');

const prefix = {
  critical: '𝙎 CRITICAL ·',
  error:    '𝙎 ERROR ····',
  warn:     '𝙎 WARN ·····',
  info:     '𝙎 INFO ·····',
  debug:    '𝙎 DEBUG ····',
  silly:    '𝙎 SILLY ····'
};

const levels = ['critical', 'error', 'warn', 'info', 'debug', 'silly'];

let level = levels.indexOf(process.env.SNAPP_LOGLEVEL);
if (level === -1) {
  level = 2;
}

const print = level => function(...args) {
  console.log(prefix[levels[level]], ...args.map(x => formatArg(x, true)));
};

module.exports = function (...args) {
  console.log(...args.map(x => formatArg(x, true)));
};

const CRITICAL = module.exports.CRITICAL = level >= 0 ? print(0) : null;
const ERROR    = module.exports.ERROR    = level >= 1 ? print(1) : null;
const WARN     = module.exports.WARN     = level >= 2 ? print(2) : null;
const INFO     = module.exports.INFO     = level >= 3 ? print(3) : null;
const DEBUG    = module.exports.DEBUG    = level >= 4 ? print(4) : null;
const SILLY    = module.exports.SILLY    = level >= 5 ? print(5) : null;

DEBUG && DEBUG('Log levels:', levels.slice(0, level + 1).map(x => x.toUpperCase()).join(', '));
