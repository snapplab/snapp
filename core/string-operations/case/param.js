const {RegularExpressions} = require('../../../');

module.exports = function () {
  return this
    .string
    .replace(RegularExpressions.case.transition.lowerToUpper, '$1-$2')
    .toLowerCase();
};
