let formatArg = (x, raw=false) => {
  if (x instanceof module.exports) {
    return x.args.map(formatArg).join(', ');
  }
  if (Array.isArray(x)) {
    return `[${x.map(formatArg).join(', ')}]`;
  }
  switch (typeof x) {
    case 'string':
      return raw ? x : `'${x}'`;
    case 'undefined':
      return '‹undefined›';
    case 'symbol':
      return `‹${x.toString()}›`;
    case 'object':
      return x === null ? 'null' : x.toString();
    default:
      return `${x}`;
  }
};

module.exports = class Arguments {
  static construct(args, name) {
    let instance = new Arguments(args, name);
    return new Proxy(instance, {
      get: function (target, propertyName) {
        if (propertyName === Symbol.toPrimitive || propertyName === 'inspect') {
          return function () {
            return this.originalArgs.map(arg => {
              if (typeof arg === 'string') {
                return `'${arg}'`;
              }
              try {
                if (typeof arg.descriptionOf === 'function') {
                  return arg.descriptionOf();
                }
              }
              catch (e) {}
              let desc = Object.keys(arg).map(key => {
                return [key, typeof arg[key]].join(': ');
              }).join(', ');
              return `{${desc}}`;
            }).join(', ');
          };
        }
        let result = target[propertyName];
        if (typeof result === 'function' && result.name !== '$$$captureArguments$$$') {
          result = result.bind(target);
        }
        else if (typeof result === 'undefined') {
          if (!propertyName.match(/^[A-Z]/)) {
            throw new Error('Type names should begin with an uppercase letter: ' + propertyName);
          }
          let type = global.SnappFramework[propertyName];
          result = instance.a(type);
        }
        return result;
      }
    });
  }

  constructor(args, name='') {
    this.originalArgs = args.slice();
    this.args = args;
    this.name = name;
  }

  toString() {
    let desc = this.originalArgs.map(formatArg).join(', ');
    return `${this.name}(${desc})`;
  }

  verifyNoUnusedArguments() {
    if (this.args.length > 0) {
      let desc = this.args.map(formatArg).join(', ');
      throw new Error(`Unused argument${this.args.length === 1 ? '' : 's'} ${desc} at ${this}`);
    }
  }

  match(desc, cond, multiple=false) {
    let matches = [];
    for (let i = 0; i < this.args.length; i++) {
      let x = this.args[i];
      if (cond(x)) {
        this.args.splice(i, 1);
        i -= 1;
        if (multiple) {
          matches.push(x);
        }
        else {
          return x;
        }
      }
    }

    if (multiple && (this.isOptional || matches.length > 0)) {
      return matches;
    }

    else if (this.isOptional) {
      return null;
    }

    throw new Error(`Required ${desc} argument not found at ${this}`);
  }

  get optional() {
    if (!this.optionalArgs) {
      this.optionalArgs = Arguments.construct(this.args, this.name);
      this.optionalArgs.isOptional = true;
    }
    return this.optionalArgs;
  }

  get string() {
    return this.match('string', x => typeof x === 'string');
  }

  get object() {
    return this.match('object', x => typeof x === 'object');
  }

  get number() {
    return this.match('number', x => typeof x === 'number' && !isNaN(x));
  }

  get integer() {
    return this.match('integer', x => typeof x === 'number' && x === parseInt(x));
  }

  get function() {
    let name = this.name;
    let fn = this.match('function', x => typeof x === 'function');
    if (fn.name === '$$$captureArguments$$$') {
      return fn;
    }
    return function $$$captureArguments$$$(...args) {
      let argsInstance = Arguments.construct(args, `${name}<function>`);
      let result = fn.call(argsInstance);
      argsInstance.verifyNoUnusedArguments();
      return result;
    };
  }

  get boolean() {
    return this.match('boolean', x => typeof x === 'boolean');
  }

  get strings() {
    return this.match('string', x => typeof x === 'string', true);
  }

  get objects() {
    return this.match('object', x => typeof x === 'object', true);
  }

  get numbers() {
    return this.match('number', x => typeof x === 'number' && !isNaN(x), true);
  }

  get integers() {
    return this.match('integer', x => typeof x === 'number' && x === parseInt(x), true);
  }

  get functions() {
    return this.match('function', x => typeof x === 'function', true);
  }

  get booleans() {
    return this.match('boolean', x => typeof x === 'boolean', true);
  }

  integerRange(min=-Infinity, max=Infinity) {
    return this.match(`${min} ≤ integer ≤ ${max}`, x =>
      typeof x === 'number' && x === parseInt(x) && x >= min && x <= max);
  }

  numberRange(min=-Infinity, max=Infinity) {
    return this.match(`${min} ≤ number ≤ ${max}`, x =>
      typeof x === 'number' && x >= min && x <= max);
  }

  a(type) {
    return this.match(type, x => x && x.constructor === type);
  }

  an(type) {
    return this.match(type, x => x && x.constructor === type);
  }
};

module.exports.formatArg = formatArg;
