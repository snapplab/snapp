const fs = require('fs');
const pkg = require('./package.json');
const Log = {CRITICAL, INFO} = require('./core/log');

const packages = {
  Log,
  Versions: {}
};

const cwd = process.cwd();

INFO && INFO(`𝙎𝖓𝖆𝖕𝖕 ${pkg.version}`);

process.on('uncaughtException', err => {
  let stack = err.stack
    .split('\n').filter((x, i) => {
      return true &&
        x.indexOf(err.constructor.name) !== 0 &&
        x.indexOf('(module.js') === -1 &&
        x.indexOf('at startup (node.js') === -1 &&
        x.indexOf('at node.js') === -1 &&
        x.indexOf('(internal') === -1
    })
    .map(x => '            ' + x.substring(2))
    .map(x => x.replace(' Object.<anonymous>', ''))
    .map(x => x.replace(cwd, '.'))
    .map(x => x.replace('./node_modules/', 'module '))
    .join('\n');
  CRITICAL && CRITICAL(`${err.constructor.name}: ${err.message}\n${stack}`);
  process.exit(1);
})

try {
  fs.readdirSync(process.env.SNAPP_HOME);
  INFO && INFO(`SNAPP_HOME is ${process.env.SNAPP_HOME}`);
}

catch (e) {
  throw new Error(`Environment variable SNAPP_HOME is not a directory: currently ${process.env.SNAPP_HOME} - set this before using Snapp.`);
}

let loaderImplementation = require('./core/zero-loader');

let loader = function (target, name) {
  return loaderImplementation(name);
};

let restrict = function (target, name, value) {
  if (name in packages.Versions) {
    let message = `${name} version previously set to ${packages.Versions[name]}`;
    throw new Error(message);
  }
  packages.Versions[name] = value;
};

global.SnappFramework = module.exports = new Proxy(packages, {get: loader, set: restrict});
loaderImplementation = module.exports.Framework.package.load(packages);

packages.Framework = loaderImplementation('Framework');
packages.StringOperations = loaderImplementation('StringOperations');
packages.RegularExpressions = loaderImplementation('RegularExpressions');
