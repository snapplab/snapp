# Snapp Framework for JavaScript

## Installation

```shell
npm install --save @snapp/framework
```

## Quick Start

### Code `hello.js`

```javascript
const {HelloWorld} = require('@snapp/framework');

HelloWorld();
```

### Run

```shell
$ mkdir -p /usr/local/snapp
$ SNAPP_LOGLEVEL=info SNAPP_HOME=/usr/local/snapp node hello.js
𝙎 INFO ····· 𝙎𝖓𝖆𝖕𝖕 0.0.6
𝙎 INFO ····· SNAPP_HOME is /usr/local/snapp
𝙎 INFO ····· Loaded package ✰ HelloWorld
"Hello World" – Snapp Framework
```

Alternatively, to use Snapp globally:

```shell
$ npm i -g @snapp/framework
$ export SNAPP_HOME=/usr/local/snapp
$ mkdir -p /usr/local/snapp
$ snapp HelloWorld
"Hello World" – Snapp Framework
```

## Framework Usage

The following environment variables control the Snapp Framework, especially when loading packages:

```
SNAPP_HOME .............. Where to install packages locally for this project,
                          can be shared with other projects without any
                          issues. Usually one location per device.

SNAPP_REPOSITORY_PATH ... Where to locate Snapp packages via the filesystem.

SNAPP_REPOSITORY ........ Where to locate Snapp packages via the internet,
                          defaults to 'https://repository.snapplab.com'.
```

To run a project built with Snapp Framework, set the environment variables when running your project with Node.

```shell
SNAPP_HOME=/usr/local/snapp node hello.js
```

## CLI Usage

To use the `snapp` command line interface, `npm install -g @snapp/framework` and add `export PATH="$PATH:/usr/local/lib/node_modules/@snapp/framework/bin"` to your shell config file.

Then use `snapp Welcome` to run the `Welcome` module.
